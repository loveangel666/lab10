
import java.util.Scanner;


//Найти, каких букв, гласных или согласных, больше в каждом предложении текста.
public class Lab10 {
    public static int glas(String s){
        char[] g = {'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'Y', 'y'};
        char[] arrayOfChar = s.toCharArray();
        int glasn = 0; //кол-во гласных
        int i = 0;
        int j = 0;
        while (i < arrayOfChar.length){
            if ( g[j] == arrayOfChar[i] ) { glasn++; i++; j=0; }
            if( j >= g.length-1 ){ j=0; i++; }
            j++;
        }
        return glasn;
    }
     
        public static int sogl(String s){
        char[] g = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'};
        char[] arrayOfChar = s.toCharArray();
        int sogla = 0; //кол-во гласных
        int i = 0;
        int j = 0;
        while (i < arrayOfChar.length){
            if ( g[j] == arrayOfChar[i] ) { sogla++; i++; j=0; }
            if( j >= g.length-1 ){ j=0; i++; }
            j++;
        }
        return sogla;
    }

    public static String sravn (int glasn, int sogla){
        String s = "Cогласных букв больше";
        if (sogla < glasn) { 
            s = "Гласных букв больше";
        } else if (sogla == glasn) {
                  s = "Согласных и гласных букв поровну";
               }
        return s;
    }
    public static void main(String[] args) {
        
        Scanner sc1 = new Scanner(System.in);
        System.out.println( "Введите строку:");
        String str = sc1.nextLine();
        //String str = "Hey Mr.Rabbit in a hurry and late. Don’t you know what it’s gonna take. Go ask Alice! She will know. What it’s like to fall down in your hole. AAAAAAARGHH";
        str = str.toLowerCase();
                        
        String[] s = str.split("[.!?]\\s*"); //отличное РВ для разбиения текста на предложения
                
        int[] n = new int[s.length]; //массив для запоминания кол-ва вхождения слова в текст.
        
                for ( int i = 0; i < s.length; i++) {
            s[i] = s[i].trim(); //обрубаем первые и последние пробелы
            System.out.println(s[i]);
        }
        
        for ( int i = 0; i < s.length; i++) {
            System.out.println("В предложении " + s[i] + " гласных букв " + glas(s[i]) + ", а согласных - " + sogl(s[i]) + ". " + sravn(glas(s[i]), sogl(s[i])));
                
        }
        
        
    }    
       
}

